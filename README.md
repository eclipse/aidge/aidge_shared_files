# aidge_shared_files
[_TOC_]
This repository holds files common to the repositories of the Aidge group.
For more informations on how the ci works [head to the dedicated page in the wiki.](https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/Understand-the-CI-CD-Pipelines)

## Content of this repository
Most files are in the .gitlab folder which contains :
1. `*.gitlab-ci.yml` files
2. Issue & Merge requests templates

The CI files contain all the ci jobs that are used in most repositories. Here is the file tree : 
```console
.gitlab
├── ci
│   ├── default.gitlab-ci.yml
│   ├── download
│   │   ├── download.gitlab-ci.yml
│   │   ├── list_dependencies.gitlab-ci.yml
│   │   └── select_jobs.gitlab-ci.yml
│   ├── release
│   │   ├── cibuildwheel_ubuntu.gitlab-ci.yml
│   │   ├── cibuildwheel_windows.gitlab-ci.yml
│   │   ├── pip.gitlab-ci.yml
│   │   └── template.gitlab-ci.yml
│   ├── rules.gitlab-ci.yml
│   ├── static_analysis
│   │   ├── cpp.gitlab-ci.yml
│   │   └── python.gitlab-ci.yml
│   ├── ubuntu_cpp.gitlab-ci.yml
│   ├── ubuntu_python.gitlab-ci.yml
│   ├── windows_cpp.gitlab-ci.yml
│   └── windows_python.gitlab-ci.yml
├── issue_templates
│   ├── Default.md
│   ├── Documentation.md
│   └── Feature.md
└── merge_request_templates
    └── Default.md
```

### **ci**
Contains all the common jobs for aidge pipelines.

- **`default`**,**`rules`** & **`download/*`** Contains templating jobs. They are not executed (because their names are prepended with a `.`) and either serve as :
1. Template for other jobs. These templates are invoked with following syntax :
  ```yml
  extends: 
    - .job:name:here
    - .other:job:name
  ```
2. Re-usable code snippets (call that a function if you want) within script.
  ```yml
	- ! reference[ .job:name:here , job_part ] # job part usually is "script", "before_script" or "after_script" 
  ```

 - **`release/*` :** Contains all jobs related to release
 - **`static_analysis/*` :** Contains all jobs related to static_analysis

### **issue && merge_requests_templates**
Contains all templates for issues & merge requests of `aidge_group`.

## Use the files to create jobs in your repository
They are ordered by platform & language. If you want to enable ci jobs for a specific platform & language,
you simply need to do 2 things : 
1. Ensure that the aidge dependencies of your repository are well listed in `.gitlab/ci/download/list_dependencies.gitlab-ci.yml`
> Do that for the windows and ubuntu jobs

2. Create a `.gitlab-ci.yml` file in your repository that follows the template given below.

```yml
###############################################################################
#                 Aidge Continuous Integration and Deployment                 #
#                                                                             #
###############################################################################

stages:
  - static_analysis
  - build
  - test
  - coverage
  - release
  - deploy

include:
  - project: 'eclipse/aidge/gitlab_shared_files' # do not modify
    ref: 'main' # branch to reference in the repo mentionned above, do not change unless you are developping on this repository and want to make tests
    file: 
      # choose which jobs to run on your ci by including the corresponding files
      - '.gitlab/ci/ubuntu_python.gitlab-ci.yml'
      - '.gitlab/ci/ubuntu_cpp.gitlab-ci.yml'

      ##############################
      # RELEASE JOBS
      # If you declared the release & deploy stages, you need to include one of the following files
      # This repository builds platform dependent packages (with .so in it because it uses pybind) hence, simple pip release is not enough, it needs cibuildwheel.
      - '.gitlab/ci/release/cibuildwheel_ubuntu.gitlab-ci.yml'   
      # - '.gitlab/ci/release/pip.gitlab-ci.yml'  

      ##############################
      # WINDOWS JOBS
      # This repository doesn't builds on windows, hence these files are not imported.
      # - '.gitlab/ci/windows_cpp.gitlab-ci.yml'

      # - '.gitlab/ci/windows_python.gitlab-ci.yml'   
      # - '.gitlab/ci/release/cibuildwheel_windows.gitlab-ci.yml'   
```

