###################################################################################################
#                                              UBUNTU
###################################################################################################

.ubuntu:download:select_jobs:
  # This task will retrieve specific jobs and store them in
  # NOTE: this script do not work in standalone
  # it is meant to be called as part of the
  # following jobs :
  # - .ubuntu:download:repositories
  # - .ubuntu:download:artifacts:
  script:
    - GITLAB_ECLIPSE_URL="https://gitlab.eclipse.org"
    - API_URL="$GITLAB_ECLIPSE_URL/api/v4"
    - echo "CI project namespace $CI_PROJECT_NAMESPACE"
    - echo "${DEPS_NAMES[@]}"
    - DEPS_SELECTED_JOBS='[]'
    - >
      for DEP_NAME in "${DEPS_NAMES[@]}"; do
        echo "current dep $DEP_NAME"

        ######################################################################################################
        # Retrieve the ID of the project
        # Depending on wether the project is the official project or a user fork
        echo "*************************************************************************"
        echo "*************************************************************************"
        echo "**"
        echo "**              RETRIEVING ${DEP_NAME^^}'S PROJECT ID"
        echo "**"
        echo "*************************************************************************"
        echo "*************************************************************************"
        if [[ $CI_PROJECT_NAMESPACE =~ "eclipse/aidge" ]]; then # nominal case : we are in the official project
          echo "curling from \"$API_URL/groups/$CI_PROJECT_NAMESPACE_ID/projects?search=${DEP_NAME}\""
          DEP_ID=$(curl -s "$API_URL/groups/$CI_PROJECT_NAMESPACE_ID/projects?search=${DEP_NAME}" | jq -r '.[0].id')
        else # case for fork
          echo "Retrieving build_artifacts from user forked project."
          echo "curling from \"$API_URL/users?username=$CI_PROJECT_ROOT_NAMESPACE\""
          USER=$(curl -s "$API_URL/users?username=$CI_PROJECT_ROOT_NAMESPACE")
          USER_ID=$(jq -r '.[0].id' <<<$USER)
          echo "curling from \"$API_URL/users/$USER_ID/projects?search=${DEP_NAME}\""
          PROJ=$(curl -s "$API_URL/users/$USER_ID/projects?search=${DEP_NAME}")
          DEP_ID=$(jq -r '.[0].id' <<<$PROJ)
        fi
        if [[ "$DEP_ID" == "null" ]]; then
          echo "$DEP_NAME not found within $API_URL/users/$USER_ID/projects."
          echo "Please make sure there is no typo in the writing and, if you are using a fork, make sure you have forked $DEP_NAME."
          exit 1
        fi
        echo "Retrieved DEP_ID=$DEP_ID"
        DEP_API_URL="$API_URL/projects/$DEP_ID"

        ######################################################################################################
        # Retrieve the branch to pull
        # Depending on wether we are in a merge request or not
        echo "*************************************************************************"
        echo "*************************************************************************"
        echo "**"
        echo "**              CHOOSING WHICH BRANCH / RELEASE / REF TO PULL"
        echo "**"
        echo "*************************************************************************"
        echo "*************************************************************************"
        DEFAULT_BRANCH="dev"           # default branch to pull
        BRANCH_TO_PULL=$DEFAULT_BRANCH # default branch to pull
        echo "Default branch to pull : $BRANCH_TO_PULL"
        echo "Choosing a branch / tag to pull dependending if we are in a Non draft merge request or not."
        echo "CI_COMMIT_TAG = $CI_COMMIT_TAG"
        echo "CI_MERGE_REQUEST_ID = $CI_MERGE_REQUEST_ID"
        echo "CI_MERGE_REQUEST_TITLE = $CI_MERGE_REQUEST_TITLE"

        ################################
        # CASE NON DRAFT MERGE REQUEST
        if [[ ! -z "$CI_MERGE_REQUEST_ID" && "$CI_MERGE_REQUEST_TITLE" != *'Draft'* ]]; then # case we are in a merge request
          echo "Retrieving branches of $DEP_NAME \"$DEP_API_URL/repository/branches?per_page=100\""
          DEP_BRANCHES=$(curl -s "$DEP_API_URL/repository/branches?per_page=100")
          # If the MR is not in draft: pulling from target branch (if exists otherwise dev)
          echo "NON DRAFT MERGE REQUEST Pipeline detected : Pulling from CI_MERGE_REQUEST_TARGET_BRANCH_NAME \"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME\"."
          echo "Checking if CI_MERGE_REQUEST_TARGET_BRANCH_NAME \"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME\" branch exists, otherwise pull from default."
          FILTERED_BRANCHES=$(jq --arg branch "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" 'map(select( .name == $branch ))' <<<"$DEP_BRANCHES")
          if [[ $(jq length <<<$FILTERED_BRANCHES) -gt 0 ]]; then
            echo "Found MR target branch in \"$DEP_NAME\"."
            echo "Pulling from merge request target branch \"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME\""
            BRANCH_TO_PULL=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
          else
            echo "Branch \"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME\" not found on repo \"$DEP_NAME\"."
            echo "Pulling from default branch : \"$BRANCH_TO_PULL\""
          fi

        ######################################
        # CASE WHERE A COMMIT TAG IS CREATED
        elif [[ ! -z "$CI_COMMIT_TAG" ]]; then
          # retrieving data from current project
          echo "Retrieving tags of $DEP_NAME via url $DEP_API_URL/repository/tags?per_page=100."
          DEP_TAGS=$(jq --slurp '
              .[]
              | sort_by(.commit.created_at)
              | reverse
              ' <<<$(curl -s "$DEP_API_URL/repository/tags?per_page=100"))

          if [[ $(jq length <<< $DEP_TAGS) == 0 ]]; then
            echo "No tag found for $DEP_NAME at url $DEP_API_URL/repository/tags?per_page=100"
            echo "Leaving early."
            exit 1
          else
            BRANCH_TO_PULL=$(jq -r '.[0].name' <<<"$DEP_TAGS")
            echo "Retrieving latest release tag : $BRANCH_TO_PULL in $DEP_NAME."
          fi
          echo "Found release : $BRANCH_TO_PULL. Using it as \$BRANCH_TO_PULL."

        ###########################################
        # CASE CASUAL COMMIT or Draft MR PIPELINE
        else
          # checking if a branch with same name exists in $DEP_NAME, otherwise, pulling from DEV
          echo "Retrieving branches of $DEP_NAME \"$DEP_API_URL/repository/branches?per_page=100\""
          DEP_BRANCHES=$(curl -s "$DEP_API_URL/repository/branches?per_page=100")
          echo "Looking for branch $CI_COMMIT_BRANCH in repo $DEP_NAME"
          FILTERED_BRANCHES=$(jq --arg branch "$CI_COMMIT_REF_NAME" 'map(select( .name == $branch ))' <<<"$DEP_BRANCHES")
          if [[ $(jq length <<<$FILTERED_BRANCHES) -gt 0 ]]; then
            echo "Found branch \"$CI_COMMIT_REF_NAME\" for repo \"$DEP_NAME\"."
            echo "Pulling from branch \"$CI_COMMIT_REF_NAME\""
            BRANCH_TO_PULL=$CI_COMMIT_REF_NAME
          else
            echo "No branch \"$CI_COMMIT_REF_NAME\" found for repo \"$DEP_NAME\"."
            echo "Pulling from branch \"$BRANCH_TO_PULL\""
          fi
        fi

        echo "*************************************************************************"
        echo "*************************************************************************"
        echo "**"
        echo "**         RETRIEVING LATEST SUCCESSFUL PIPELINES IN GIVEN BRANCH"
        echo "**"
        echo "*************************************************************************"
        echo "*************************************************************************"

        ######################################################################################################
        # Retrieve latest successful $DEPENDENCY_JOB from $BRANCH_TO_PULL (including MR pipelines)
        # and sort them from newest to oldest
        echo "Retrieving pipelines from $BRANCH_TO_PULL"
        echo "Regular pipeline curled from \"$DEP_API_URL/pipelines?ref=${BRANCH_TO_PULL}\""
        PIPELINES=$(curl -s "$DEP_API_URL/pipelines?ref=${BRANCH_TO_PULL}")
        if [[ -z "$PIPELINES" ]]; then
          echo "Failed to retrieve any pipeline, ensure there is a pipeline with ref $BRANCH_TO_PULL in $DEP_NAME"
          exit 1
        fi
        echo "Found $(jq length <<<$PIPELINES) regular pipelines with $BRANCH_TO_PULL as ref."

        echo "Retrieve first affiliated MR by curling \"$DEP_API_URL/merge_requests?source_branch=$BRANCH_TO_PULL&state=opened\""
        MR_ID=$(curl -s "$DEP_API_URL/merge_requests?source_branch=$BRANCH_TO_PULL&state=opened" | jq '.[0].iid')
        if [[ $MR_ID != null ]]; then
          echo "MR pipeline curled from \"$DEP_API_URL/merge_requests/$MR_ID/pipelines\""
          MR_PIPELINES=$(curl -s "$DEP_API_URL/merge_requests/$MR_ID/pipelines")
          echo "Found $(jq length <<<$MR_PIPELINES) Merge Request pipelines with $BRANCH_TO_PULL as source_branch."
        else
          echo "No merge request with source_branch = \"$BRANCH_TO_PULL\" found."
          MR_PIPELINES="[]"
        fi
        set -x
        PIPELINES=$(
          jq -n \
            --argjson pipelines "$PIPELINES" \
            --argjson mr_pipelines "$MR_PIPELINES" \
            --arg project_namespace "$CI_PROJECT_NAMESPACE" \
            '$pipelines + $mr_pipelines
            | map(select(.web_url | test($project_namespace)))
            | sort_by(.updated_at)
            | reverse'
        )
        if [[ ! -z "$CI_COMMIT_TAG" ]]; then
          PIPELINES=$(jq 'map(select(.status == "success"))' <<<"$PIPELINES")
        fi

        echo "*************************************************************************"
        echo "*************************************************************************"
        echo "**"
        echo "**               ITERATING OVER EACH PIPELINES"
        echo "**                   FROM NEWEST TO OLDEST"
        echo "**    TO FIND LATEST SUCCESSFUL JOB OF TYPE \"$DEPENDENCY_JOB\""
        echo "**"
        echo "*************************************************************************"
        echo "*************************************************************************"
        FOUND_JOB=0
        # iterate over each pipeline and get the job given job
        for i in $(seq 0 $(($(jq length <<<$PIPELINES) - 1))); do
          PIPELINE_ID=$(jq --argjson idx $i '.[$idx].id' <<<$PIPELINES)
          echo "Current pipeline ID : $PIPELINE_ID"
          echo "Retrieving jobs of curr pipeline from : \"$DEP_API_URL/pipelines/$PIPELINE_ID/jobs\""
          JOBS=$(curl -s "$DEP_API_URL/pipelines/$PIPELINE_ID/jobs")
          if [[ $(jq -r '.message' <<<$JOBS) == "404 Not found" ]]; then
            echo "WARNING: NO JOBS FOUND FOR PIPELINE $PIPELINE_ID OF PROJECT $DEP_NAME."
            echo "WARNING: This might happen if on $DEP_NAME / $BRANCH_TO_PULL:"
            echo "WARNING:    - There is an ongoing pipeline"
            echo "WARNING:    - A branch was jsut merged in just merged to $BRANCH_TO_PULL."
            echo "WARNING: IGNORING THIS PIPELINE."
            echo "WARNING: To solve this issue either wait for pipeline to finish or re-run a pipeline on given branch."
            continue
          fi
          # Retrieve jobs only if they are successful
          # Only retrieve part of the json for readability
          echo "Retrieving only $DEPENDENCY_JOB that are successful."
          echo "Retrieving only part of the job json for readbility"
          set -x
          JOBS="$(jq --arg job_name "$DEPENDENCY_JOB" \
            --arg branch_name "$BRANCH_TO_PULL" \
            --arg repo_name "$DEP_NAME" \
            --arg repo_id "$DEP_ID" \
            '[
                          map(select((.name == $job_name) and (.status == "success"))) |
                          .[] |
                          {
                              repo : $repo_name,
                              repo_id : $repo_id,
                              branch_to_pull : $branch_name,
                              id, status, name, ref, created_at, updated_at,
                              user : { id : .user.id, username : .user.username },
                              commit : { author : .commit.author_name, id : .commit.id, message : .commit.message, authored_date : .commit.authored_date },
                              pipeline : { id : .pipeline.id, iid : .pipeline.iid, project_id : .pipeline.project_id, ref : .pipeline.ref},
                            }
                        ]' <<<"$JOBS")"
          if [[ $(jq length <<<$JOBS) > 0 ]]; then
            FOUND_JOB=1
            echo "Successful job \"$DEPENDENCY_JOB\" found in branch \"$BRANCH_TO_PULL\" for project \"$DEP_NAME\""
            echo "Appending this jobs to the list of selected jobs that will serve as reference to retrieve artifact or specific commit."
            # concatenate json
            DEPS_SELECTED_JOBS=$(jq --argjson result_json "$(jq '.[0]' <<<$JOBS)" '. + [$result_json]' <<<"$DEPS_SELECTED_JOBS")
            break
          fi
        done

        if [[ $FOUND_JOB == 0 ]]; then
          echo "ERROR : no successful job \"$DEPENDENCY_JOB\" found in branch \"$BRANCH_TO_PULL\" for project \"$CI_PROJECT_NAMESPACE/$DEP_NAME\""
          if [[ ! -z "$CI_MERGE_REQUEST_ID" && $CI_MERGE_REQUEST_TITLE != *'Draft'* ]]; then
            echo "*************************************************************************"
            echo "*************************************************************************"
            echo "**"
            echo "** WARNING: This is a NON DRAFT Merge request Pipeline:"
            echo "** WARNING: Pulled branch:"
            echo "** WARNING: CI_MERGE_REQUEST_TARGET_BRANCH_NAME $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
            echo "** WARNING: If the Merge Request were in draft, pulled branch would have been:"
            echo "** WARNING: CI_MERGE_REQUEST_SOURCE_BRANCH_NAME $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME."
            echo "** WARNING:"
            echo "** WARNING: If you encounter issues running this pipeline:"
            echo "** WARNING: 1. Make sure that dependencies' merge requests are merged (if any)"
            echo "** WARNING: 2. Pass your MR in draft if they are not merged."
            echo "**"
            echo "*************************************************************************"
            echo "*************************************************************************"
          fi
          exit -1
        fi
      done
    - echo "*************************************************************************"
    - echo "*************************************************************************"
    - echo "*"
    - echo "*                        FINAL SELECTED JOBS"
    - echo "*"
    - echo "*************************************************************************"
    - echo "*************************************************************************"
    - echo "SELECTED_JOBS = $(jq '.' <<< "$DEPS_SELECTED_JOBS")"


###################################################################################################
#                                              WINDOWS
###################################################################################################
.windows:download:select_jobs:
  # dowload 1 dependency
  # Note:
  # For this script to work you need to define teh following variables
  #  - DEPENDENCY_NAME: Name of the dependency project
  #  - DEPENDENCY_JOB: Name of the dependency job from which you want to pull artifacts
  script:
    - $DEPS_SELECTED_JOBS = @()
    - >
      foreach ($DEP_NAME in $DEPS_NAMES) {
        Write-Host "curr dep = $DEP_NAME"
        # Download dependencies

        ######################################################################################################
        # Retrieve the ID of the project
        # Depending on wether the project is the official project or a user fork
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "**"
        Write-Host "**               RETRIEVING DEPENDENCY $DEP_NAME ID"
        Write-Host "**"
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "CI_PROJECT_NAMESPACE = $CI_PROJECT_NAMESPACE"
        if ( $CI_PROJECT_NAMESPACE -match ".*eclipse/aidge.*" ){ # nominal case : we are in the official project
            Write-Host "Retrieve DEP ID from $API_URL/groups/$CI_PROJECT_NAMESPACE_ID/projects?search=$DEP_NAME"
            $DEP_ID=$( Invoke-RestMethod -Uri "$API_URL/groups/$CI_PROJECT_NAMESPACE_ID/projects?search=$DEP_NAME" -Method Get)[0].id
        } else {
            # case for fork
            Write-Host "Forked repo detected : not on the official repo but on a user's forked project."
            Write-Host "Retrieving USER_ID from : $API_URL/users?username=$CI_PROJECT_ROOT_NAMESPACE"
            $USER_ID=$(Invoke-RestMethod -Uri "$API_URL/users?username=$CI_PROJECT_ROOT_NAMESPACE" -Method Get)[0].id
            Write-Host "USER ID = $USER_ID"
            $DEP_ID=$(Invoke-RestMethod -Uri "$API_URL/users/$USER_ID/projects?search=${DEP_NAME}" -Method Get)[0].id
        }
        Write-Host "DEP_ID = $DEP_ID"
        $DEP_API_URL="$API_URL/projects/$DEP_ID"

        ######################################################################################################
        # Retrieve the branch to pull
        # Depending on wether we are in a merge request or not
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "**"
        Write-Host "**               CHOOSING WHICH BRANCH TO PULL"
        Write-Host "**"
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        $DEFAULT_BRANCH="dev"
        $BRANCH_TO_PULL=$DEFAULT_BRANCH # default branch to pull
        Write-Host "Choosing a branch to pull dependending if we are in a Non draft merge request or not."
        Write-Host "CI_COMMIT_TAG = $CI_COMMIT_TAG"
        Write-Host "CI_MERGE_REQUEST_TITLE = $CI_MERGE_REQUEST_TITLE"

        ################################
        # CASE NON DRAFT MERGE REQUEST
        if ( "$CI_MERGE_REQUEST_ID" -and ! ("$CI_MERGE_REQUEST_TITLE" -match 'Draft.*') ) { # case we are in a merge request
            # If the MR is not in draft: pulling from target branch (if exists otherwise dev)
            Write-Host "Retrieving branches of $DEP_NAME $DEP_API_URL/repository/branches?per_page=100"
            $DEP_BRANCHES=$($(Invoke-RestMethod -Uri "$DEP_API_URL/repository/branches?per_page=100" -Method Get) | Select-Object name)
            Write-Host "Found $($DEP_BRANCHES.Count) branches."
            Write-Host "$($DEP_BRANCHES | Out-String)"
            Write-Host "NON DRAFT MERGE REQUEST Pipeline detected : Pulling from CI_MERGE_REQUEST_TARGET_BRANCH_NAME : $CI_MERGE_REQUEST_TARGET_BRANCH_NAME."
            Write-Host "Checking if CI_MERGE_REQUEST_TARGET_BRANCH_NAME "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" branch exists, otherwise pull from default."
            $FILTERED_BRANCHES=@($DEP_BRANCHES | Where-Object { $_.name -eq "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"})

            if ( ($FILTERED_BRANCHES.Count) -gt 0){
                Write-Host "Found MR target branch in $DEP_NAME."
                $BRANCH_TO_PULL=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
                Write-Host "Pulling from merge request target branch $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
            } else {
                Write-Host "Branch $CI_MERGE_REQUEST_TARGET_BRANCH_NAME not found on repo $DEP_NAME."
                Write-Host "Pulling from default branch."
            }

        ######################################
        # CASE WHERE A COMMIT TAG IS CREATED
        } elseif ( "$CI_COMMIT_TAG" ) {
          Write-Host "Retrieving repo tags from $DEP_API_URL/repository/tags?per_page=100"
          $DEP_TAGS = $($(Invoke-RestMethod -Uri "$DEP_API_URL/repository/tags?per_page=100" -Method Get) |
              Sort-Object -Property {[datetime]$_.commit.created_at} -Descending)
          if ( $DEP_TAGS.Count -eq 0 ) {
            Write-Host "No tag found for $DEP_NAME at url $DEP_API_URL/repository/tags?per_page=100"
            Write-Host "Leaving early."
            exit 1
           } else {
            $BRANCH_TO_PULL=$DEP_TAGS[0].name
            Write-Host "Retrieving latest available release : v$BRANCH_TO_PULL."
          }
        ###########################################
        # CASE CASUAL COMMIT or Draft MR PIPELINE
        } else {
          Write-Host "Retrieving branches of $DEP_NAME $DEP_API_URL/repository/branches?per_page=100"
          $DEP_BRANCHES=$($(Invoke-RestMethod -Uri "$DEP_API_URL/repository/branches?per_page=100" -Method Get) | Select-Object name)
          Write-Host "Found $($DEP_BRANCHES.Count) branches."
          Write-Host "$($DEP_BRANCHES | Out-String)"
          # checking if a branch with same name exists in $DEP_NAME, otherwise, pulling from dev
          Write-Host "No non draft merge request, looking for branch CI_COMMIT_REF_NAME : $CI_COMMIT_REF_NAME"
          $FILTERED_BRANCHES=@($DEP_BRANCHES | Where-Object { $_.name -eq "$CI_COMMIT_REF_NAME"})
          if ( $FILTERED_BRANCHES.Count -gt 0  ) {
            Write-Host "Found branch $CI_COMMIT_REF_NAME for repo $DEP_NAME."
            $BRANCH_TO_PULL=$CI_COMMIT_REF_NAME
          } else{
            Write-Host "No branch $CI_COMMIT_REF_NAME found for repo $DEP_NAME."
            Write-Host "Pulling from default branch."
          }
        }
        Write-Host "Pulling from branch $BRANCH_TO_PULL"

        #######################################################
        # Retrieve JOB ID
        ## Prepare pipelines to parse to retrieve the jobs of each pipelines
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "**"
        Write-Host "**           RETRIEVING LATEST SUCCESSFUL PIPELINES IN REF"
        Write-Host "**                          $BRANCH_TO_PULL"
        Write-Host "**"
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "Retrieving regular pipelines from $DEP_API_URL/pipelines?ref=$BRANCH_TO_PULL"
        $PIPELINES=$(Invoke-RestMethod -Uri "$DEP_API_URL/pipelines?ref=$BRANCH_TO_PULL" -Method Get)
        Write-Host "Found $($PIPELINES.Count) pipelines linked with $BRANCH_TO_PULL as ref."
        Write-Host "Retrieving Merge requests pipelines from $DEP_API_URL/merge_requests?source_branch=$BRANCH_TO_PULL&state=opened"
        $MR_ID=(Invoke-RestMethod -Uri "$DEP_API_URL/merge_requests?source_branch=$BRANCH_TO_PULL&state=opened" -Method Get)
        Write-Host "Found $($MR_ID.Count) opened MR with $BRANCH_TO_PULL as source branch."
        if ( $MR_ID.Count -ne 0 ){
            $MR_PIPELINES=(Invoke-RestMethod -Uri "$DEP_API_URL/merge_requests/$($MR_ID[0].iid)/pipelines" -Method Get)
            $PIPELINES+=$MR_PIPELINES
        }
        Write-Host "Pipelines retrieved:"
        $PIPELINES | Out-String

        Write-Host "Sorting pipelines from newest to oldest & filtering out those not linked to CI_PROJECT_NAMESPACE ($CI_PROJECT_NAMESPACE)"
        $PIPELINES = $PIPELINES |
                     Where-Object { $_.web_url -match $CI_PROJECT_NAMESPACE } |
                     Sort-Object -Property updated_at -Descending
        $PIPELINES = @($PIPELINES)
        Write-Host "Final Pipelines nb to parse : $($PIPELINES.Count)"
        $PIPELINES | Out-String
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "**"
        Write-Host "**             ITERATING OVER EACH PIPELINES"
        Write-Host "**               FROM NEWEST TO OLDEST"
        Write-Host "**         TO FIND LATEST SUCCESSFUL JOB OF TYPE $DEPENDENCY_JOB"
        Write-Host "**"
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        $FOUND_JOB=0
        for($i = 0; $i -lt $PIPELINES.Count ; $i++){
            $PIPELINE = $PIPELINES[$i]
            Write-Host "current pipeline : $i = $PIPELINE"
            Write-Host "curling from $DEP_API_URL/pipelines/$($PIPELINE.id)/jobs"
            $JOBS=$($(Invoke-RestMethod -Uri "$DEP_API_URL/pipelines/$($PIPELINE.id)/jobs?name=$DEPENDENCY_JOB" -Method Get))
            # Filter jobs by name
            $JOBS = @(
                    $JOBS |
                    Where-Object { $_.name -eq $DEPENDENCY_JOB -and $_.status -eq "success" } |
                    Select-Object @{Name="repo"; Expression={$DEP_NAME}},
                        id, status, @{Name="job_name"; Expression={$_.name}},  ref,
                        @{Name="branch_to_pull"; Expression={$BRANCH_TO_PULL}},
                        created_at, updated_at,
                        @{Name="user"; Expression={[PSCustomObject]@{id=$_.user.id; username=$_.user.username}}},
                        @{Name="commit"; Expression={[PSCustomObject]@{id=$_.commit.short_id; message=$_.commit.message; authored_date=$_.commit.authored_date}}},
                        @{Name="pipeline"; Expression={[PSCustomObject]@{id=$_.pipeline.id; iid=$_.pipeline.iid; project_id=$_.pipeline.project_id; ref=$_.pipeline.ref}}}
                )

            if ( $JOBS.Count -gt 0 ) {
                $FOUND_JOB=1
                Write-Host "Successful job $DEPENDENCY_JOB found in branch $BRANCH_TO_PULL for project $DEP_NAME"
                Write-Host "Appending this jobs to the list of selected jobs that will serve as reference to retrieve artifact or specific commit."
                $DEPS_SELECTED_JOBS+=$($JOBS[0])
                break
            }
        }
        if ( $FOUND_JOB -eq 0 ) {
            Write-Host "ERROR : no successful job $DEPENDENCY_JOB found in branch $BRANCH_TO_PULL for project $CI_PROJECT_NAMESPACE/$DEP_NAME"
            if ( ! "$CI_MERGE_REQUEST_ID" -and ! "$CI_MERGE_REQUEST_TITLE" -match  '.+Draft.+' ){
              Write-Host "*************************************************************************"
              Write-Host "*************************************************************************"
              Write-Host "**"
              Write-Host "** WARNING: This is a NON DRAFT Merge request Pipeline:"
              Write-Host "** WARNING: Pulled branch :"
              Write-Host "** WARNING: CI_MERGE_REQUEST_TARGET_BRANCH_NAME $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
              Write-Host "** WARNING: If the merge request were in draft, pulled branch would have been:"
              Write-Host "** WARNING: CI_MERGE_REQUEST_SOURCE_BRANCH_NAME $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME."
              Write-Host "** WARNING:"
              Write-Host "** WARNING: If you encounter issues running this pipeline:"
              Write-Host "** WARNING: 1. Make sure that dependencies' merge requests are merged (if any)"
              Write-Host "** WARNING: 2. Pass your MR in draft if they are not merged."
              Write-Host "**"
              Write-Host "*************************************************************************"
              Write-Host "*************************************************************************"
            }
            exit -1
        }

        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "*"
        Write-Host "*                   FINAL SELECTED JOBS"
        Write-Host "*"
        Write-Host "*************************************************************************"
        Write-Host "*************************************************************************"
        Write-Host "SELECTED_JOBS = $($DEPS_SELECTED_JOBS | Out-String)"
      }

