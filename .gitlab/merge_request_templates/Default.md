## Context

(Summarize the problem you are facing and the solution you propose to resolve the issue)

(If possible add link to a gitlab issue #...)

(Describe the issue - you can paste any logs or/screenshots, the link to the line of code that might be responsible for the problem)

(In the case of feature addition, describe the context arround the need of this addition)

## Modified files

(Summarize the changes you made for each modified file - example:
- `DeepNet.hpp` and `DeepNet.cpp`, add several learning methods;
- `learn.cpp`, to include the learning functions inside the main function;
- `test_conv.cpp`, to correct the new syntax in the tests; )

## Detailed major modifications

(Explain the non-trivial changes you made in the modified files)

(Specify if you made an important technical decision which should be added to the wiki https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/home or the documentation)


## TODO

(List the changes that need to be done to track the progress of the MR, you need to update the list when committing !)

- [ ] NOT DONE
- [X] DONE
- [ ] TO DO
