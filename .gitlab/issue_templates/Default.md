## Required prerequisites

- [ ] Make sure you've read the documentation. Your issue may be addressed there.
- [ ] Search the issue tracker and discussions to verify that this hasn't already been reported. +1 or comment there if it has.

## What commit version of aidge do you use
- ``aidge_core``: X.X.X
- ``aidge_...``: X.X.X

## Problem description

Define the expected behavior.

Is this a regression ?

Please provide logs in the form of a code block

```bash
Logs ...
```

## Reproducible example code

You can attach code or use code block

```python
```

or

```c++
```

/label ~Issue::Bug 🐛
