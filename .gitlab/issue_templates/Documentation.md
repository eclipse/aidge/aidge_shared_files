## Faulty documentation

(Present here what you think is missing/not up to date in the documentation)

[link to the faulty documentation if possible]($PATH_TO_DOC)

## Possible Fixes

(If you can, suggest a possible fix)


/label ~Add::Documentation 📚
