#!/bin/bash
set -e
set -x 

#################
# THIS SMALL CODE IS MEANT TO HELP DEBUG CI BASH SCRIPT LOCALLY
#################
## DO NOT MODIFY
GITLAB_ECLIPSE_URL="https://gitlab.eclipse.org/"
API_URL="https://gitlab.eclipse.org/api/v4"

## SETUP CI VARIABLES
CI_PROJECT_ROOT_NAMESPACE='eclipse/aidge'
CI_PROJECT_NAMESPACE="eclipse/aidge"
CI_PROJECT_NAMESPACE_ID="6437"
CI_PROJECT_NAME="aidge_backend_cuda"
CI_COMMIT_BRANCH="fix"

USER=$(curl -s "${API_URL}/users?username=${CI_PROJECT_ROOT_NAMESPACE}")
## CREATE YOUR ENV HERE
PROJECT=$(curl -s "${API_URL}/users/${USER_ID}/projects?search=${DEP_NAME}")
DEPS_SELECTED_JOBS='[]'
DEP_NAME="aidge_core"
DEP_ID=5140 
# PIPELINE_ID=51499
# BRANCH_TO_PULL="dev"
# DEPENDENCY_JOB="job name here"
# CI_PROJECT_ID=5190
DEP_API_URL="$API_URL/projects/$DEP_ID"

####  COPY PASTE CODE FROM CI SCRIPTS HERE TO RUN THEM LOCALLY
